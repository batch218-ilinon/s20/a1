// console.log("Hello World");

let numberInput = prompt("Number: ");
console.log("The number you provided is " + numberInput);
for(let i = numberInput; i > 0; i--){
	if(i <= 50){
		console.log("The current value is " + i + ". Terminating the loop.")
		break;
	};
	if(i % 10 == 0){
		console.log("The number is divisible by 10. Skipping the number.");
	};
	if(i % 5 == 0 && i % 10 != 0){
		console.log(i);
	}
};

let word = "supercalifragilisticexpialidocious";
let consonants = [];
let consonantCount = 0;
console.log(word);
for(let i = 0; i < word.length; i++){
    if(
        word[i] == "a" ||
        word[i] == "e" ||
        word[i] == "i" ||
        word[i] == "o" ||
        word[i] == "u" 
    ){
        continue;
    };
    	consonants[consonantCount] = word[i];
    	consonantCount++;
};
let consonantString = consonants.join("");
console.log(consonantString);

